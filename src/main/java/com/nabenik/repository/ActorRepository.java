package com.nabenik.repository;

import com.nabenik.model.Actor;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ActorRepository {

    @Inject
    EntityManager em;

    public Actor findById(Long id){
        return em.find(Actor.class, id);
    }

    public void create(Actor actor){
        em.persist(actor);
    }

    public Actor update(Actor actor){
        return em.merge(actor);
    }

    public void deleteById(Long id) {
        Actor actor = em.find(Actor.class, id);
        if(actor != null) {
            em.remove(actor);
        }
    }

    public List<Actor> listAll(String name){

        String query = "SELECT a FROM Actor a " +
                "where a.name LIKE :name";

        Query typedQuery = em.createQuery(query)
                .setParameter("name", "%".concat(name).concat("%"));

        return typedQuery.getResultList();
    }

}
