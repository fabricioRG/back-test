package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ActorRepositoryTest {

    @Inject
    ActorRepository actorRepository;

    @Deployment
    public static WebArchive createDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(ActorRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");
        return war;
    }

    @Test
    public void listAll() {
        //Arrange
        String title = "Watchmen";
        List<Actor> actors = getActorList(1,title);
        for (Actor act :
                actors) {
            actorRepository.create(act);
        }
        int expect = actors.size();
        //Act
        List<Actor> result = actorRepository.listAll(title);
        //Assert
        System.out.println("Expect: " + expect);
        System.out.println("Result: " + result.size());
        assertEquals(expect,result.size());
    }

    @Test
    public void findById() {
        //Arrange
        Actor expect = getActorList(1,"Anne Hathaway").get(0);
        actorRepository.create(expect);
        //Act
        Actor result = actorRepository.findById(expect.getActorId());
        //Assert
        assertEquals(expect.getName(),result.getName());
    }

    @Test
    public void create() {
        //Arrange
        Actor actor = new Actor();
        actor.setName("Emma Stone");
        //Act
        actorRepository.create(actor);
        //Assert
        assertNotNull(actor.getActorId());
    }

    @Test
    public void update() {
        //Arrange
        Actor actor = getActorList(1,"Jared Letto").get(0);
        actorRepository.create(actor);
        String expect = actor.getName();
        //Act
        actor.setCountry("Canada");
        Actor result = actorRepository.update(actor);
        //Assert
        assertNotEquals(expect,result.getCountry());
    }

    @Test
    public void deleteById() {
        //Arrange
        Actor actor = getActorList(1,"Brad Pitt").get(0);
        actorRepository.create(actor);
        Long actorId = actor.actorId;
        //Act
        actorRepository.deleteById(actorId);
        Actor result = actorRepository.findById(actorId);
        //Assert
        assertNull(result);
    }

    private List<Actor> getActorList(int size, String name){
        List actorList = new ArrayList();
        for (int i = 0; i < size; i++) {
            Actor act = new Actor();
            act.setName(name);
            actorList.add(act);
        }
        return actorList;
    }
}
