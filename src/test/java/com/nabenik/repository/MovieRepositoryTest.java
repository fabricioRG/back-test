package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class MovieRepositoryTest {

    @Inject
    MovieRepository movieRepository;

    @Deployment
    public static WebArchive createDeployment(){
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(MovieRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");

        System.out.println(war.toString(true));

        return war;
    }

    @Test
    public void create() {
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.create(movie);


        System.out.println("Movie Id " + movie.getMovieId());

        assertNotNull(movie.getMovieId());
    }

    @Test
    public void findById(){
        //Arrange
        Movie expect = getMovieList(1,"Interstellar").get(0);
        movieRepository.create(expect);
        //Act
        Movie result = movieRepository.findById(expect.getMovieId());
        //Assert
        assertEquals(expect.getTitle(),result.getTitle());
    }

    @Test
    public void update(){
        //Arrange
        Movie movie = getMovieList(1,"Mr Nobody").get(0);
        movieRepository.create(movie);
        String expect = movie.getYear();
        //Act
        movie.setYear("2010");
        Movie result = movieRepository.update(movie);
        //Assert
        assertNotEquals(expect,result.getTitle());
    }

    @Test
    public void deleteById(){
        //Arrange
        Movie movie = getMovieList(1,"World War Z").get(0);
        movieRepository.create(movie);
        Long movieId = movie.getMovieId();
        //Act
        movieRepository.deleteById(movieId);
        Movie result = movieRepository.findById(movieId);
        //Assert
        assertNull(result);
    }

    @Test
    public void listAll(){
        //Arrange
        String title = "Watchmen";
        List<Movie> movies = getMovieList(1,title);
        for (Movie mv :
                movies) {
            movieRepository.create(mv);
        }
        int expect = movies.size();
        //Act
        List<Movie> result = movieRepository.listAll(title);
        //Assert
        assertEquals(expect,result.size());
    }

    private List<Movie> getMovieList(int size,String name){
        List movieList = new ArrayList();
        for (int i = 0; i < size; i++) {
            movieList.add(new Movie(name,"2000","2 hours"));
        }
        return movieList;
    }

}
