# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* JavaScript knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations

- JSON-P

> Allow JSON manipulations on the text, which requests to a remote service

- CDI

> It allows us to manage contexts and inject components into objects

- JPA

> It's a data access layer of an application, and describes the management of relational data  

- JAX-RS

> It's an API for RESTful Web Services, and implements support for annotations and interfaces

3. Which of the following is/are not an application server?

* Helidon
* Quarkus
* WebSphere
* Tomcat
* KumuluzEE

> Helidon, Quarkus, Tomcat, KumuluzEE

4. In your opinion what's the main benefit of moving from Java 8 to Java 11

> Types inference, static methods, and garbage collector

5. In your opinion what's the main benefit of using TypeScript over JavaScript

> TypeScript is an Object-oriented programming language, while JavaScript is a scripting language

6. What's the main difference between OpenJDK and HotSpot

> Hotspot has some extra enterprise features, and patching are before for Hotspot 

8. If no database is configured? Will you be able to run this project? Why?

> Yes, but depends. If you prefer to deploy your application in Payara,
> this brings a default database, so, with a few of steps you should be available
> to run the project.

## Development tasks

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11 and  NodeJS 16
![Alt text](assets/easy-0.png?raw=true "easy 0")

1. (easy) Build this project on a regular CLI over OpenJDK 11
![Alt text](assets/easy-1-2.png?raw=true "easy 1")
![Alt text](assets/easy-1-1.png?raw=true "easy 1")

2. (easy) Run this project using an IDE/Editor of your choice
![Alt text](assets/easy-2-1.png?raw=true "easy 2")
![Alt text](assets/easy-2-2.png?raw=true "easy 2")

3. (medium) This project has been created using Java EE APIs, please identify at least four APIs, bump it to Java EE 8 and Java 11, later run it over regular Payara Application Server
   > JPA, EJB, JTA, Bean Validation

   ![Alt text](assets/medium-3-1.png?raw=true "easy 2")
   ![Alt text](assets/medium-3.png?raw=true "easy 2")

4. (medium) Execute the movie endpoint operations using a client -e.g. Postman-, if needed please also check/fix the code to be REST compliant
   > Create

   ![Alt text](assets/medium-4-create.png?raw=true "easy 2")

   > Read

   ![Alt text](assets/medium-4-find-1.png?raw=true "easy 2")
   ![Alt text](assets/medium-4-find-2.png?raw=true "easy 2")

   > Update
   
   ![Alt text](assets/medium-4-update-1.png?raw=true "easy 2")
   ![Alt text](assets/medium-4-update-2.png?raw=true "easy 2")

   > Delete
   
   ![Alt text](assets/medium-4-delete-1.png?raw=true "easy 2")
   ![Alt text](assets/medium-4-delete-2.png?raw=true "easy 2")

6. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer  
   
   [fabricioRG/simple-crud-angular](https://bitbucket.org/fabricioRG/simple-crud-angular/src/main/)

7. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods

   > Arquillian: src/test/java/com/nabenik/repository/MovieRepositoryTest.java

   ![Alt text](assets/hard-6.png?raw=true "easy 2")

8. (hard) Based on `MovieRepository` Integration Test, please create an integration test using [RestAssured](https://rest-assured.io/) and `MovieController`

9. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test

   ![Alt text](assets/hard-8-1.png?raw=true "easy 2")

10. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Oracle Helidon](https://helidon.io/). Do it and don't port Integration Tests 

